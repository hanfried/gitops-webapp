FROM scratch
LABEL maintaner="Janek Schleicher <xxx@nospam.org>"
COPY . .
EXPOSE 8080
CMD ["./main"]
